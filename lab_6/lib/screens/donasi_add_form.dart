import 'package:flutter/material.dart';

class AddDonasiForm extends StatefulWidget {
  const AddDonasiForm({Key? key}) : super(key: key);

  @override
  _AddDonasiFormState createState() => _AddDonasiFormState();
}

class _AddDonasiFormState extends State<AddDonasiForm> {
  final _formKey = GlobalKey<FormState>();
  String textFieldsValue = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
        ),
        body:  SingleChildScrollView(
            child: Padding(padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                child: Column(
                  children: [
                    Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            TextFormField(
                              autofocus: true,
                              decoration: new InputDecoration(
                                hintText: "masukan nama lengkap anda",
                                labelText: "Nama Lengkap",
                                icon: Icon(Icons.people),
                                border: OutlineInputBorder(
                                    borderRadius: new BorderRadius.circular(5.0)),
                              ),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Nama tidak boleh kosong';
                                }
                                textFieldsValue = value;
                                return null;
                              },
                            ),
                            RaisedButton(
                              child: Text(
                                "Submit",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Colors.blue,
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  print(textFieldsValue);
                                }
                              },
                            ),
                          ],
                        )
                    )
                  ],
                )
            )
        )
    );
  }
}
