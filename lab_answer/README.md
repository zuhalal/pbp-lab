## No 1
### Apakah perbedaan antara JSON dan XML?
JSON merupakan Javascript Object Notation, JSON memiliki bentuk seperti objek pada javascript dan memiliki sepasang key dan value di dalamnya. JSON digunakan untuk mengirim dan menerima data pada web, aplikasi, ataupun _mobile_. XML merupakan Extensible Markup Language. XML memiliki bentuk dengan masing-masing data memiliki tag pembuka dan penutup. Sama seperti JSON, JSON digunakan untuk mengirim dan menerima data pada web, aplikasi, ataupun mobile.

Perbedaan keduanya adalah sebagai berikut: 
- JSON memiliki bentuk seperti objek javascript sedangkan XML memiliki bentuk seperti informasi yang dibungkus di dalam tag. 
- XML merupakan bahasa markup sedangkan JSON hanyalah format data.
- JSON bersifat _self describing_ sedangkan XML bersifat _self descripting_.
- XML disimpan dalam bentuk tree structure sedangkan JSON disimpan dalam bentuk objek yang memiliki _key_ dan _value
_

## No 2
### Apakah perbedaan antara HTML dan XML?
Perbedaan antara HTML dan XML adalah sebagai berikut:
- HTML digunakan untuk menampilkan informasi di browser melalui tag-tag html sedangkan XML digunakan untuk penerimaan dan pertukaran data dalam bentuk tag.
- HTML _insensitive case_ sedangkan XML _sensitive case_
- HTML menyediakan dukungan namespaces sedangkan XML tidak


### Referensi
- https://www.monitorteknologi.com/perbedaan-json-dan-xml/
- https://blogs.masterweb.com/perbedaan-xml-dan-html/

