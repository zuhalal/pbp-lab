# Generated by Django 3.2.7 on 2021-09-21 13:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0005_friend_no_telp'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dosen',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=30)),
                ('matkul', models.CharField(max_length=30)),
            ],
        ),
        migrations.RemoveField(
            model_name='friend',
            name='no_telp',
        ),
    ]
