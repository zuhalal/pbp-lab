# Generated by Django 3.2.7 on 2021-09-21 13:50

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('lab_1', '0002_alter_friend_npm'),
    ]

    operations = [
        migrations.AddField(
            model_name='friend',
            name='no_telp',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
