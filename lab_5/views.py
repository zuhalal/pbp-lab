from django.core import serializers
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import (get_object_or_404, render, HttpResponseRedirect, redirect)
import json

from lab_2.models import Note
from lab_5.forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all()
    form = NoteForm()

    if request.method == "POST":
        form  = NoteForm(request.POST)
        if form.is_valid():
            form.save()
    
    response = {'notes': notes}
    response['forma'] = form
    return render(request, 'lab5_index.html', response)

def detail_note(request, id):
    obj = Note.objects.get(id = id)
    data = serializers.serialize('json', [obj, ])

    struct = json.loads(data)
    data = json.dumps(struct[0])
    return HttpResponse(data, content_type='application/json')

def delete_note(request, id):
    obj = get_object_or_404(Note, id = id)
    
    if request.method == "POST":
        # delete object
        obj.delete()
        
    data = serializers.serialize('json', Note.objects.all())

    return HttpResponse(data, content_type='application/json')

def update_note(request, id):
    obj = get_object_or_404(Note, id = id)

    if request.is_ajax():
        new_from = request.POST.get("sender")
        new_to = request.POST.get("to")
        new_title = request.POST.get("title")
        new_message = request.POST.get("message")
        obj.to = new_to
        obj.sender = new_from
        obj.title = new_title
        obj.message = new_message
        obj.save()
        return HttpResponse(serializers.serialize('json', [obj, ]), content_type='application/json')
    # gmn caranya dptin data dri ajaxnyaaaaaaaaa????????
    # add form dictionary to context
    # some error occured
    return JsonResponse({"error": "Edit Failed"}, status=400)