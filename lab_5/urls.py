from lab_5.views import  delete_note, detail_note, index, update_note
from django.urls import path

urlpatterns = [
    path('', index, name='index_lab5'),
    path('notes/<int:id>/update', update_note, name="update_notes"),
    path('notes/<int:id>', detail_note, name="view_notes"),
    path('notes/<int:id>/delete', delete_note, name="delete_notes"),
]