// pk digunakan buat identifier unik antara satu message dengan yang lain buat yang mau kita delete
let pk = "";
$(document).ready(function () {
  $("#tablebody").on('click', '#openDelete', function() {
      // kita dapetin pk dengan cara mengambil value dari masing-masing tabel
      pk = $(this).attr("value");
    });
});
$(document).ready(function () {
  $("#delete").click(function () {
    $.ajax({
      type: 'POST',
      url: `/lab-5/notes/${pk}/delete`,
      data: {
            // csrf token for security
            'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val()
      },
      success: function (result) {
        // sebelum kita refresh tabel, kita kosongin dulu tablenya
        $(".tblbdy").empty();
        // saat sudah bersih, kita dapetin ulang datanya
        $.ajax({
            url: "/lab-2/json", 
            success: function(results){
                results.map((result)=>{
                  insertTableData(result);
                }
              )
            }
          }
        );
      },
    });
  });
});

//Digunakan agar ketika men-click tombol delete yang berupa tombol submit, ngga ngerefresh halaman
document.getElementById("delete").addEventListener("click", function(event){
  event.preventDefault()
});