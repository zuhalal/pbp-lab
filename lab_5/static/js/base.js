const insertTableData = (result) => {
  return $(".tblbdy").append(`
        <tr>
          <td id="msgSender">${result.fields.sender}</td>
          <td id="msgTo">${result.fields.to}</td>
          <td id="msgTitle">${result.fields.title}</td>
          <td id="msgNote">${result.fields.message}</td>
        <td>
          <div class="btn-action-wrapper">
            <button
              type="button"
              value="${result.pk}"
              class="btn btn-primary view"
              id="view"
            >
              View
            </button>
            <button type="button" class="btn btn-secondary"
              data-bs-toggle="modal"
              data-bs-target="#modalEdit"
              id="openUpdate"
              value="${result.pk}"
            >Edit</button>
            <button
              type="button"
              class="btn btn-danger"
              data-bs-toggle="modal"
              data-bs-target="#exampleModal"
              value="${result.pk}"
              id="openDelete"
            >
              Delete
            </button>
          </div>
        </td>
      </tr>`)
}

const getMessageData = () => {
  return $(document).ready(function(){
            $.ajax({
                url: "/lab-2/json", 
                success: function(results){
                    results.map((result)=>{
                      insertTableData(result);
                    }
                  )
                }
              }
            );
          }
        );
}

getMessageData();