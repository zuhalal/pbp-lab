// pk digunakan buat identifier unik antara satu message dengan yang lain buat yang mau kita edit
// dalam hal ini ketika kita men-click tombol Edit pada tabel
$(document).ready(function () {
  $("#tablebody").on('click', '#openUpdate', function() {
      pk = $(this).attr("value");
  });
});

// dijalankan ketika kita men-submit form
$("#form-update").submit(function(e) {
   // preventing from page reload and default actions
  e.preventDefault();

  // mendapatkan value dari masing-masing tag input
  sender_value = $("#id_sender").val()
  to_value = $("#id_to").val()
  title_value = $("#id_title").val()
  message_value = $("#id_message").val()

  $.ajax({
    type: 'POST',
    url: `/lab-5/notes/${pk}/update`,
    data: {
      // add request body with input value and csrf token
      'csrfmiddlewaretoken': $("input[name=csrfmiddlewaretoken]").val(),
      'title': title_value,
      'to': to_value,
      'sender': sender_value,
      'message': message_value
    },
    success: function (response) {
      // kita kosongin inputnya lagi abis berhasil nembak
      $("#id_sender").val("") 
      $("#id_to").val("") 
      $("#id_title").val("") 
      $("#id_message").val("") 

      $(".tblbdy").empty();
      $.ajax({
          url: "/lab-2/json", 
          success: function(results){
              results.map((result)=>{
                insertTableData(result);
              }
            )
          }
        }
      );
    },
  })
});