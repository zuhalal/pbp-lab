from django.http.response import HttpResponseRedirect
from lab_3.forms import FriendForm
from lab_1.models import Friend
from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='/admin/login/')

def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab_3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm()

    if request.method == "POST":
        form  = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/lab-3")

    response = {'form': form}
    return render(request, 'lab3_form.html', response)