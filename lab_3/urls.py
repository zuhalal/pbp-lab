from lab_3.views import add_friend, index
from django.urls import path

urlpatterns = [
    path('', index, name='index_lab3'),
    path('add/', add_friend, name="add")
]