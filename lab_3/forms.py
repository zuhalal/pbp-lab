from django.forms import widgets
from lab_1.models import Friend
from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = '__all__'
        widgets ={'tanggal_lahir': DateInput()}
        # error_messages = {
        #         'required' : 'Please Type'
        #     }

        # input_attrs = {
        #     'type' : 'text',
        # }

        # name = forms.CharField(label="name", required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs))
        # npm = forms.IntegerField(label="name", required=True, widget=forms.IntegerField(attrs=input_attrs))
        # tanggal_lahir = forms.DateField(label="name", required=True, widget=forms.DateField(attrs=input_attrs))
