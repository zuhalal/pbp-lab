from lab_2.views import index, renderJSON, renderXML
from django.urls import path

urlpatterns = [
    path('', index, name='index'),
    path('xml', renderXML, name='xml'),
    path('json', renderJSON, name='json'),
]