from django.forms import widgets
from lab_2.models import Note
from django import forms

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['sender', 'to', 'title', 'message']

    error_messages = {
            'required' : 'Tolong Isi Semua Data'
        }

    input_attrs = {
        'type' : 'text',
    }

    sender = forms.CharField(label="Sender", required=True, max_length=50, widget=forms.TextInput(attrs=input_attrs), error_messages=error_messages)
    to = forms.CharField(label="Receiver", required=True, max_length=50,widget=forms.TextInput(attrs=input_attrs), error_messages=error_messages)
    title = forms.CharField(label="Title", required=True, widget=forms.TextInput(attrs=input_attrs), error_messages=error_messages)
    message = forms.CharField(label="Message", required=True, widget=forms.Textarea(attrs=input_attrs), error_messages=error_messages)