from lab_4.views import  add_note, index, note_list
from django.urls import path

urlpatterns = [
    path('', index, name='index_lab4'),
    path('add-note/', add_note, name='add_notes'),
    path('note-list/', note_list, name='notes_list'),
]